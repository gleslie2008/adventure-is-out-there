# Adventure Is Out There! #
A Pebble App written in Pebble.js on CloudPebble.  You can [download the app from the Pebble App Store](https://apps.getpebble.com/applications/53d728183ca9f796db00002b).

## Building ##
Add the files to CloudPebble.  Create a new file, keys.js, and include a Google Places API key, like so:

```
#!javascript
var keys = {
	google_places_api_key : "secrets!",
};

this.exports = keys;
```

Enjoy!