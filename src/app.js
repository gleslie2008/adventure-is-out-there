// imports
var UI = require('ui');
var ajax = require('ajax');
var config = require('config');
var language = require('language');
var keys = require('keys');
var types = require('types');
var places = require('places');

// members

// cards
var error = new UI.Card({
	title: language.title,
	subtitle: language.error,
	scrollable: true
});

var list = new UI.Menu({
	sections: [{
    title: language.results
	}]
});

var detail = new UI.Card({
	scrollable: true
});

var searching = new UI.Card({
	title: "Looking for",
	body: "Adventure is Out There!"
});

var menu = new UI.Menu({
	sections: [{
    title: language.long_title,
	}]
});

var results = [];

// methods
function showDetail(i) {
	detail.title(results[i].name);
	detail.subtitle(results[i].rating + language.stars);
	detail.body(results[i].vicinity);
	detail.show();
}

function findType(i) {
	for (var type in types) {
		if (parseInt(types[type].id) === i) {
			return types[type];
		}
	}
}

function search(typeid) {
	var type = findType(typeid);
	searching.subtitle(type.name);
	searching.show();
	
	var radius = config.miles_to_search * config.mi_to_meters;
	
	var lat, lng;
	navigator.geolocation.getCurrentPosition(function(position) {
		lat = position.coords.latitude;
		lng = position.coords.longitude;
		
		var url = places.url;
		url += places.types + type.code;
		url += places.amp + places.location + lat + "," + lng;
		url += places.amp + places.radius + radius;
		url += places.amp + places.open;	
		console.log("request:");
		console.log(url + "&key=***");
		url += places.amp + places.key + keys.google_places_api_key;
		
		ajax({ url: url, type: 'json' }, function(data) {
			console.log("list: " + data.results.length);
			results = [];
			results = data.results;
			
			if (data.status != places.status_ok) {
				console.log("status error: " + data.status);
				error.body(language.error);
				if (data.status == places.status_zero)
					error.body(language.zero);
				if (data.status == places.status_over)
					error.body(language.over);
				
				error.show();
				return;
			}
			
			// clear the list 
			list = new UI.Menu({
				sections: [{
					title: language.results
				}]
			});
			
			var len = Math.min(results.length, config.max_results);
			for (var i = 0; i < len; i ++) {
				var name = results[i].name;
				var stars = results[i].rating + language.stars;
				list.item(0, i, { title: name, subtitle: stars });
			}
			list.on('select', function(event) {
				showDetail(event.item);
			});			
			list.show();
		});
	});
}

function main() {
	if (!navigator.geolocation) {
		error.body(language.no_location);
		error.show();
		return;
	}
	
	var i = 0;
	for (var type in types) {
		menu.item(0, i, { title: types[type].name });
		i ++;
	}
	menu.on('select', function(event) {
		search(event.item);
	});
	menu.show();
	
}

// run
main();

