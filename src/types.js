var types = {
	amusment_park : { 
		id: 0, name: "Amusment Park", code: "amusment_park" 
	},
	aquarium : { 
		id: 1, name: "Aquarium", code: "aquarium" 
	},
	art_gallery : { 
		id: 2, name: "Art Gallery", code: "art_gallery" 
	},
	bar : { 
		id: 3, name: "Bar", code: "bar" 
	},
	book_store : { 
		id: 4, name: "Book Store", code: "book_store" 
	},
	bowling_alley : { 
		id: 5, name: "Bowling Alley", code: "bowling_alley" 
	},
	cafe : { 
		id: 6, name: "Coffee Shop", code: "cafe" 
	},
	restaurant : { 
		id: 7, name: "Resturant", code: "restaurant" 
	},
	gym : { 
		id: 8, name: "Gym", code: "gym" 
	},
	movie_theater : { 
		id: 9, name: "Movie Theater", code: "movie_theater" 
	},
	museum : { 
		id: 10, name: "Museum", code: "museum" 
	},
	night_club : { 
		id: 11, name: "Night Club", code: "night_club" 
	},
	park : { 
		id: 12, name: "Park", code: "park" 
	},
	parking : { 
		id: 13, name: "Parking", code: "parking" 
	}
};

this.exports = types;