var language = {
	title : "Adventure!",
	long_title : "Adventure is Out There!",
	results : "Results",
	stars : "/5.0 stars",
	no_location : "Pebble can't find itself!  Is location on?",
	zero : "Aw man, nothing nearby was found :(",
	over : "Aw snap, we're over our Google API request limit :(",
	error: "Error!"
};

this.exports = language;