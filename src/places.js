var places = {
	url : "https://maps.googleapis.com/maps/api/place/search/json?",
	location : "location=",
	radius : "radius=",
	key : "key=",
	types : "types=",
	open : "opennow",
	amp : "&",
	status_ok : "OK",
	status_zero : "ZERO_RESULTS",
	status_over : "OVER_QUERY_LIMIT"
};

this.exports = places;